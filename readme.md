# Simulation Code for 
## Unintended Selection
### Persistent Qualification Rate Disparities and Interventions

Author: Reilly Raab (reilly@ucsc.edu)

This directory organizes python functions used to simulate the dynamical
system(s) appearing in our publication.

## Contents
```
.
├── images/             - empty directory to hold output files
├── main.py             - edit and run this file to generate plots
├── classifiers.py      - defines classifiers/interventions
├── responses.py        - defines population response functions
├── setting.py          - object representing bundled parameters
├── system.py           - object bundling setting, classifier, and response
├── state.py            - object representing point in phase space
├── plot.py             - defines organization of matplotlib figures
└── util.py             - misc. utility functions
```

## Dependencies

library versions used during development:
 - CPython 3.9.5
 - numpy 1.20.3
 - scipy 1.7.1
 - matplotlib 3.4.2
 - tqdm 4.62.2
 
Plots appearing in the paper were each generated in less than 30 seconds on a
single-threaded on a 2.6 GHz x64 CPU

## Instructions

Edit main.py as necessary to specify desired system and plot parameters (see
commented examples).


``` sh
python3 main.py
```
